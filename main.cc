#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#include <functional>
using namespace std;

typedef struct tagStats {
	unsigned int counts[ 53 ];
	float probabilities[ 53 ];
	int Q1, Q2, Q3;
	int mode;
} Stats;

Stats stats[6];

vector<unsigned int> col1, col2, col3, col4, col5, col6;
void parseLottoNumber( const string &num );
template <class AnyType>
void calculateProbabilities( unsigned int col, const vector<AnyType> &list );
const vector<unsigned int> findMaximums( const float probabilities[], unsigned int howMany );


int main( )
{
	cout << "Florida Lottery Statistical Analysis" << endl;
	cout << "=====================================" << endl;
	ifstream in;
	ofstream out;
	in.open( "fl_lotto.txt", ios_base::in );
	in.seekg( 15 );
	string numbers = "", date = "", day = "";

	// Build number lists...
	while( !in.eof( ) )
	{
		numbers = "";
		date = "";
		day = "";
		
		in >> numbers;
		in >> date;
		in >> day;
		parseLottoNumber( numbers );
		//cout << numbers << " \t\t" << date << " " << day << endl;
	}
	in.close( );
	

	// Calculate Probabilities...
	calculateProbabilities( 1, col1 );
	calculateProbabilities( 2, col2 );
	calculateProbabilities( 3, col3 );
	calculateProbabilities( 4, col4 );
	calculateProbabilities( 5, col5 );
	calculateProbabilities( 6, col6 );

	// Output the statistical data...
	out.open( "fl_lotto_stats.txt" );
	ostringstream oss;
	Stats *stat;

	out << "Florida Lottery Statistical Analysis" << endl;
	out << "=====================================\n" << endl;
	
	out << "Out of " << col1.size( ) << " winning lottery numbers." << endl;
	out << "Lottery number: A-B-C-D-E-F\n" << endl;

	for( unsigned int cnt = 1; cnt <= 6; cnt++ )
	{
		out << "Column " << (char)(cnt + 0x40) << " Statistics" << endl;

		stat = &stats[ cnt - 1 ];

		out << "Number" << " \t\tNbr of Occurences\tProbability" << endl;
		for( unsigned int n = 1; n <= 53; n++ )
		{
			//out.width( 30 );
			out.setf( out.adjustfield );

			out.precision( 2 );
			out << n << " \t\t" << stat->counts[ n - 1 ] << "\t\t\t" << 100 * stat->probabilities[ n - 1 ] << "%" << endl; 
		}
		
		out << "Good candidates?  ";
		const vector<unsigned int> &maxs = findMaximums( stat->probabilities, 5 );
		vector<unsigned int>::const_iterator itr = maxs.begin( );
		for( int i = 0; i < 4; i++, ++itr )
			out << *itr << ", ";
		out << *itr << endl;

		out << endl << endl;
	}





	out.close( );
	
	return 0;
}

void parseLottoNumber( const string &num )
{
	string acc = "";

	for( unsigned int i = 0, cnt = 1; i < num.size( ); i++ )
	{
		if( num[ i ] == '-' )
		{
			switch( cnt )
			{
				case 1:
					col1.push_back( atol( acc.c_str( ) ) );
					break;
				case 2:
					col2.push_back( atol( acc.c_str( ) ) );
					break;
				case 3:
					col3.push_back( atol( acc.c_str( ) ) );
					break;
				case 4:
					col4.push_back( atol( acc.c_str( ) ) );
					break;
				case 5:
					col5.push_back( atol( acc.c_str( ) ) );
					break;
				case 6:
					col6.push_back( atol( acc.c_str( ) ) );
					break;
				default:
					cout << "ERROR: parseLottoNumber( ), got more than 5 numbers..." << endl;
			}
			acc = "";
			cnt++;
		}
		else {
			acc += num[ i ];
		}
	}
	// last number is here
	col6.push_back( atol( acc.c_str( ) ) );
}

template <class AnyType>
void calculateProbabilities( unsigned int col, const vector<AnyType> &list )
{
	unsigned int hits = 0,
				 size = list.size( );

	for( int n = 1; n <= 53; n++ )
	{
		hits = 0;
		for( int idx = 0; idx < size; idx++ )
		{
			if( list[ idx ] == n )
				hits++;
		}
		stats[ col - 1 ].counts[ n - 1 ] = hits;
		stats[ col - 1 ].probabilities[ n - 1 ] = ((float) hits) / ((float) size);
	}
}

const vector<unsigned int> findMaximums( const float probabilities[], unsigned int howMany )
{
	vector<unsigned int> max;
	vector< pair<float, unsigned int> > prob;

	for( int i = 1; i <= 53; i++ )
		prob.push_back( pair<float, unsigned int>( probabilities[ i - 1 ], i ) );



    sort( prob.begin( ), prob.end( ), greater< pair<float, unsigned int> >( ) );

	for( int i = 0; i < 5; i++ )
		max.push_back( prob[ i ].second );

	return max;
}